package mobcast.samplecardstackview;

/**
 * Created by amansatija on 3/31/17.
 */

public class ModelPerson {
    public String mStrName = "amn satija";
    public String mStrEmail = "satija.aman@gmail.com";


    public ModelPerson(String mStrArgName,String email ){
        this.mStrEmail = email;
        this.mStrName = mStrArgName;
    }
}
