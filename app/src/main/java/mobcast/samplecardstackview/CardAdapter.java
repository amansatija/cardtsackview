package mobcast.samplecardstackview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CardAdapter extends ArrayAdapter<ModelPerson> {
    ArrayList<ModelPerson> mModelPersonArrayList = new ArrayList<>();

    public CardAdapter(Context context, ArrayList<ModelPerson> mArgArrayList) {
        super(context, 0);
        if(mArgArrayList!=null)
        this.mModelPersonArrayList = mArgArrayList;
    }


    @Override
    public void add(@Nullable ModelPerson person) {
        mModelPersonArrayList.add(person);
    }

    @Override
    public int getCount() {
        if (mModelPersonArrayList != null)
            return mModelPersonArrayList.size();
        else
            return 0;
    }


    @Nullable
    @Override
    public ModelPerson getItem(int position) {
        return mModelPersonArrayList.get(position);
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        ViewHolder holder;

        if (contentView == null) {
            contentView = View.inflate(getContext(), R.layout.item_card_stack, null);
            holder = new ViewHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (ViewHolder) contentView.getTag();
        }

        ModelPerson mperson = getItem(position);

        holder.textView.setText(mperson.mStrName);
        holder.email.setText(mperson.mStrEmail);
        return contentView;
    }

    private static class ViewHolder {
        public TextView textView;

        public TextView email;
        public ViewHolder(View view) {
            this.textView = (TextView) view.findViewById(R.id.item_card_stack_text);
            this.email = (TextView)view.findViewById(R.id.item_card_stack_text_email);
        }
    }

}
